package com.ushakova.tm.exception.auth;

import com.ushakova.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("An error has occurred: access is denied.");
    }

}
