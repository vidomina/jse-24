package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.model.Project;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {
}
