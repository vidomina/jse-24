package com.ushakova.tm.comparator;

import com.ushakova.tm.api.entity.IHasCreated;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByCreated implements Comparator<IHasCreated> {

    @NotNull
    private final static ComparatorByCreated INSTANCE = new ComparatorByCreated();

    @NotNull
    public static ComparatorByCreated getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasCreated o1, @Nullable final IHasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateCreate().compareTo(o2.getDateCreate());
    }

}
