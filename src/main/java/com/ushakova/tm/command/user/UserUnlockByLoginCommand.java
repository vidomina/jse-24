package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserUnlockByLoginCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Unlock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("***Unlock User***");
        System.out.println("***Enter Login:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
        System.out.println("***Ok**");
    }

    @Override
    @NotNull
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
