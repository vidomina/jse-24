package com.ushakova.tm.command;

import com.ushakova.tm.api.service.IServiceLocator;
import com.ushakova.tm.enumerated.Role;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @NotNull
    public abstract String name();

    @Nullable
    public Role[] roles() {
        return null;
    }

}
